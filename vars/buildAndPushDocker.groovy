#!/usr/bin/env groovy

def call() {
    // Use credentials to authenticate with Docker Hub
    withCredentials([usernamePassword(credentialsId: 'devmentat-dockerhub', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
        // Build and push the Docker image using the PACKAGE_BUILD_VERSION environment variable
        sh """docker build -t devmentat/training:${PACKAGE_BUILD_VERSION} .
        echo ${PWD} | docker login -u "${USER}" --password-stdin
        docker push devmentat/training:${PACKAGE_BUILD_VERSION}"""
        // Export as ENV image name
        env.IMAGE = "devmentat/training:${PACKAGE_BUILD_VERSION}"
    }
}
