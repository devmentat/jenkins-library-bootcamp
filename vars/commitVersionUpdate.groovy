#!/usr/bin/env groovy

def call( String gitUrl, String sshCredentialsId, String lang) {
    // Use SSH key to authenticate with Git
    sshagent([sshCredentialsId]) {
        // Set the git user email and name
        sh 'git config --global user.email "jenkins@devmentat.com"'
        sh 'git config --global user.name "jenkins"'

        // Set the git remote URL
        sh "git remote set-url origin ${gitUrl}"

        // Add and commit the changes
        sh "git add ."
        // Read the PACKAGE_BUILD_VERSION environment variable and use it in the commit message
        def packageBuildVersion = env.PACKAGE_BUILD_VERSION ?: 'unknown version'
        sh "git commit -a -m \"[ci skip] chore: bump to v${packageBuildVersion} - ${lang} ${langVersion}\""

        // Push the changes to the remote repository
        sh 'git push origin HEAD:refs/heads/main'
    }
}