#!/usr/bin/env groovy

def call() {
    withCredentials([usernamePassword(credentialsId: 'devmentat-ec2-ip', usernameVariable: 'EC2_IP_USERNAME', passwordVariable: 'EC2_IP_ADDRESS')]) {
        // Docker command
        def dockerCompUp = 'docker-compose up -d'
        sshagent(['devmentat-ec2-ssh']) {
            // Send docker compose file
            sh "scp -o StrictHostKeyChecking=no docker-compose.yml ${EC2_IP_USERNAME}@${EC2_IP_ADDRESS}"
            // Start docker compose
            sh "ssh -o StrictHostKeyChecking=no ${EC2_IP_USERNAME}@${EC2_IP_ADDRESS} ${dockerCompUp}"
        }
    }
}
