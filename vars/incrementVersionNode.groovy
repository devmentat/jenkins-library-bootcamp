#!/usr/bin/env groovy

def call(String appDirectory) {
    // Change to the "app" directory and run the "npm version minor" command to increment the package version
    dir(appDirectory) {
        sh "npm version minor"

        // Read the package.json file and extract the version number
        def packageVersion = sh(script: "cat package.json | grep version | head -n 1 | awk -F '\"' '{print \$4}'", returnStdout: true).trim()
        echo "${packageVersion}"

        // Set the PACKAGE_BUILD_VERSION environment variable to the combination of the package version and the Jenkins build number
        env.PACKAGE_BUILD_VERSION = "${packageVersion}-${env.BUILD_NUMBER}"
        echo "PACKAGE_BUILD_VERSION=${env.PACKAGE_BUILD_VERSION}"

        // Run the "node -v" command to get the Node.js version
        def langVersion = sh(script: "node -v", returnStdout: true).trim()
        echo "${langVersion}"

        // Set the LANGVERSION environment variable to the Node.js version
        env.LANGVERSION = langVersion
        echo "LANGVERSION=${env.LANGVERSION}"
    }
}
