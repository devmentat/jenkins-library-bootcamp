#!/usr/bin/env groovy

def call(String appDirectory) {
    try {
        dir(appDirectory) {
            sh "npm ci"
            sh "npm run test"
        }
    } catch (Exception e) {
        echo "Failed to run tests: ${e}"
        throw e
    }
}
